# Diario lezioni

Nota bene: a lezione non è detto si riesca ad affrontare tutti gli argomenti, ma per l'esame vanno conosciuti i capitoli del libro elencati nel README.

## Lezioni (in ordine cronologico inverso)

### Future

* 2020-03-27 (v) primi passi GPIO

### Passate

* 2020-03-25 (v) legge di Ohm, resistenze
* 2020-03-20 (v) primi passi IDE
* 2020-03-18 (v) intro sistemi embedded, fino a "conversione ad/da escluso"
* 2020-03-13 (v) intro corso, prove tecniche video conf

## Argomenti da evadere (non in ordine di presentazione, non riusciremo a trattare tutti i topic in aula)

FIXME cfr. file lista domande
